package com.theara.onlineshopping.controller;

import com.theara.onlineshopping.dto.request.PaymentRequest;
import com.theara.onlineshopping.dto.response.PaymentResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/secure/payment")
public class PaymentController {

    @PostMapping("")
    public PaymentResponse makePayment(@RequestBody PaymentRequest request) {

        if ("4111111111111111".equals(request.getCreditCardNumber())) {
            return PaymentResponse
                    .builder()
                    .success(true)
                    .message("Successful payment")
                    .build();
        } else {
            return PaymentResponse
                    .builder()
                    .success(false)
                    .message("Failed payment")
                    .build();
        }
    }

}
