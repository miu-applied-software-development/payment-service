package com.theara.onlineshopping.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PaymentRequest {

    private String creditCardNumber;
    private String expirationDate;
    private String cvv;
    private String cardholderName;
    private Double paymentAmount;

}
